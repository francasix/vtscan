from flask import Flask
from flask_socketio import SocketIO
import os
import printy

CLIENT = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'client')
BUILD = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'client/dist')
TEMPLATE = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'client/templates')

app = Flask(__name__, template_folder=TEMPLATE, static_url_path=BUILD, static_folder=BUILD)
app.secret_key = 'xyz'
socketio = SocketIO(app, async_mode='threading')
