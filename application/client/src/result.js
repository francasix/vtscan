import Vue from 'vue'
import App from './Result.vue'

new Vue({
  el: '#result',
  render: h => h(App)
})

