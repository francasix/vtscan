from uuid import uuid4
from flask import redirect
from application.config import Config
import requests.auth
import requests
import constant
import printy
import urllib.request


class Auth:
    """Oauth2 authentification"""

    def __init__(self):
        self.data = []

    def make_authorization_url(self):
        # Generate a random string for the state parameter
        # Save it for use later to prevent xsrf attacks
        state = str(uuid4())
        self.save_created_state(state)

        params = {"client_id": constant.CLIENT_ID,
                  "response_type": "code",
                  "state": state,
                  "redirect_uri": constant.REDIRECT_URI,
                  "scope": constant.SCOPE}

        url = constant.AUTHORIZE_URL + urllib.parse.urlencode(params)

        # redirect to /callback
        return redirect(url)

    def save_created_state(self, state):
        pass

    def is_valid_state(self, state):
        return True

    def get_token(self, code):

        client_auth = requests.auth.HTTPBasicAuth(constant.CLIENT_ID, constant.CLIENT_SECRET)

        post_data = {"grant_type": "authorization_code",
                     "code": code,
                     "redirect_uri": constant.REDIRECT_URI}
        response = requests.post(constant.ACCESS_TOKEN_URL,
                                 auth=client_auth,
                                 data=post_data)
        token_json = response.json()

        c = Config()

        constant.TOKEN = c.update('TOKEN', token_json["access_token"])

        return redirect('/uber')
