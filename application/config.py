import os
import requests
import json

class Config:
    def __init__(self):
        self.data = []

    def update(self, name, value):
        file = os.path.abspath("token.txt")

        # save token into text file
        with open(file, 'w') as f:
            f.write(value)
            f.close()

    def token(self):
        file = os.path.abspath("token.txt")

        with open(file, 'r') as f:
            data = f.read()

        return data

    def token_exist(self):
        if (os.stat(os.path.abspath("token.txt")).st_size > 0):
            return True

        return False

    def get_geolocation(self):
        send_url = 'http://freegeoip.net/json'
        r = requests.get(send_url)
        j = json.loads(r.text)

        data_location = {
            "lat": j['latitude'],
            "lon": j['longitude']
        }

        return data_location
