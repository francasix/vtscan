import json
import urllib.parse as urlparse
from urllib.parse import urlencode

import requests

from application.config import Config
import constant as c


class Uber_parameters(json.JSONEncoder):

    def __init__(self):

        self.products = []
        self.start_lat = None
        self.start_lon = None
        self.c = Config()

    def get_products(self, lat, lon):

        self.start_lat = lat
        self.start_lon = lon

        url = "https://api.uber.com/v1.2/products"

        data = {
            "access_token": self.c.token(),
            "latitude": lat,
            "longitude": lon
        }

        try:

            # concatenation url + data parameters
            url_parts = list(urlparse.urlparse(url))

            query = dict(urlparse.parse_qsl(url_parts[4]))
            query.update(data)

            url_parts[4] = urlencode(query)
            headers = {'Authorization': 'Token ' + c.SERVER_TOKEN}

            req = requests.get(urlparse.urlunparse(url_parts), headers=headers)

        except Exception as e:
            return e

        response = json.loads(req.content.decode('utf-8'))

        for product in response['products']:
            self.products.append((product['product_id'], product['display_name']))

        return self.products

    def to_json(self):
        """
            to_json transforms the Model instance into a JSON string
        """
        return jsonpickle.encode(self)
