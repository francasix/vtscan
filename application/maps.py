import printy
import json
import constant
import requests
from application.config import Config
import urllib.parse as urlparse
from urllib.parse import urlencode

class Maps():

    def __init__(self):
        self.location = {}
        self.c = Config()

    def get_place_location(self, address):

        data = {
            "address": address,
            "key": constant.API_KEY
        }

        url = "https://maps.googleapis.com/maps/api/geocode/json"
        url_parts = list(urlparse.urlparse(url))
        query = dict(urlparse.parse_qsl(url_parts[4]))
        query.update(data)

        url_parts[4] = urlencode(query)

        try:

            req = requests.get(urlparse.urlunparse(url_parts))

        except Exception as e:
            print(e)
            return 'Error on API google maps request'

        response = json.loads(req.content.decode('utf-8'))

        self.location = {
            "latitude": response['results'][0]['geometry']['location']['lat'],
            "longitude": response['results'][0]['geometry']['location']['lng']
        }

        return self.location
