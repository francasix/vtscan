import json
import threading
import time
import urllib.parse as urlparse
from urllib.parse import urlencode

import requests
from flask import session

import printy
from application import socketio
from application.config import Config
import constant



class Uber():

    def __init__(self, start, end, address_pickup, address_dropoff):

        self.products = session.get('uber_products')
        self.rides = []
        self.time = None
        self.start_lat = None
        self.start_lon = None
        self.delay = None
        self.c = Config()

        self.start = (start[0], start[1])
        self.end = (end[0], end[1])
        self.address_pickup = address_pickup
        self.address_dropoff = address_dropoff

    def to_json(self):
        return json.dumps(self, default=lambda o: o.__dict__,
                          sort_keys=True, indent=4)

    def ride_info(self, p):

        price = self.estimate_price((self.start[0], self.start[1]), (self.end[0], self.end[1]), p[0])
        delay = self.estimate_time((self.start[0], self.start[1]), p[0])

        self.rides.append({
            "name": p[1],
            "price": price,
            "delay": delay,
            "app_url": self.universal_url(p[0])
        })

        # emit event each time rides is updated
        socketio.emit('ride', {'data': self.rides}, namespace='/ride-info')

    async def get_ride_info(self):

        for p in self.products:
            m = threading.Thread(target=self.ride_info, args=[p])
            m.daemon = True
            time.sleep(0.1)
            m.start()

    def universal_url(self, product_id):

        client_id = constant.CLIENT_ID

        url = 'https://m.uber.com/ul/?client_id='+str(client_id)+'&action=setPickup&pickup[latitude]='+str(self.start[0])+'&pickup[longitude]='+str(self.start[1])+'&pickup[formatted_address]=1455%20Market%20St%2C%20San%20Francisco%2C%20CA%2094103&dropoff[latitude]='+str(self.end[0])+'&dropoff[longitude]='+str(self.end[1])+'&dropoff[formatted_address]=1%20Telegraph%20Hill%20Blvd%2C%20San%20Francisco%2C%20CA%2094133&product_id='+str(product_id)

        printy.log(url)

        return url

    def estimate_price(self, start, end, product_id):

        self.start_lat = start[0]
        self.start_lon = start[1]

        data = {
            "start_latitude": start[0],
            "start_longitude": start[1],
            "end_latitude": end[0],
            "end_longitude": end[1],
            "product_id": product_id
        }

        headers = {'Content-Type': 'application/json'}

        url = "https://api.uber.com/v1.2/requests/estimate"
        data_url = {"access_token": self.c.token()}

        # concatenation url + data parameters
        url_parts = list(urlparse.urlparse(url))
        query = dict(urlparse.parse_qsl(url_parts[4]))
        query.update(data_url)

        url_parts[4] = urlencode(query)

        req = requests.post(urlparse.urlunparse(url_parts), data=json.dumps(data), headers=headers, stream=True)

        response = json.loads(req.content.decode('utf-8'))
        product_price = {}

        if 'fare' in response:
            # price value from request
            product_price = response['fare']['value']

        return product_price

    def estimate_time(self, start, id):

        data = {
            "start_latitude": start[0],
            "start_longitude": start[1],
            "access_token": self.c.token()
        }

        try:

            url = "https://api.uber.com/v1.2/estimates/time"

            # concatenation url + data parameters
            url_parts = list(urlparse.urlparse(url))
            query = dict(urlparse.parse_qsl(url_parts[4]))
            query.update(data)

            url_parts[4] = urlencode(query)

            req = requests.get(urlparse.urlunparse(url_parts))

        except Exception as e:
            print(e)
            return 'Error on request'

        response = json.loads(req.content.decode('utf-8'))

        # delay time from request
        for p in response['times']:
            if p['product_id'] == id:
                self.delay = int(p['estimate'] / 60)

        return self.delay
