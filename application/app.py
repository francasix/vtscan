import asyncio

from flask import abort, redirect, request, render_template, session

from application import app
from application import socketio
from application.auth import Auth
from application.config import Config
from application.maps import Maps
from application.uber import Uber
from application.uber_parameters import Uber_parameters
from flask_socketio import emit


@app.route("/")
def hello():
    return render_template('index.html')


@app.route("/start")
def start():
    a = Auth()
    c = Config()

    # geolocation based on IP
    lat = c.get_geolocation()['lat']
    lon = c.get_geolocation()['lon']

    uber = Uber_parameters()

    # list of Uber's products (Pool, X, Van etc.)
    session['uber_products'] = uber.get_products(lat, lon)

    if c.token_exist():
        return redirect('/uber')
    else:
        return a.make_authorization_url()


@app.route('/callback')
def callback():
    a = Auth()
    error = request.args.get('error', '')

    if error:
        return "Error: " + error

    state = request.args.get('state', '')

    if not a.is_valid_state(state):
        # Uh-oh, this request wasn't started by us!
        abort(403)

    try:

        code = request.args.get('code')
        return a.get_token(code)

    except Exception as e:

        print(e)
        return 'Error'


@app.route("/uber")
def uber(name=None):
    c = Config()
    token = c.token()

    return render_template('index.html', token=token)


@app.route('/estimates/time/<string:product_id>', methods=['POST'])
def delay(product_id):
    # loop through all product ids available to get delay estimation
    uber.estimate_time(session.get('uber_parameters').start_lat, session.get('uber_parameters').start_lon, product_id)

    return render_template("details.html", time=uber.time)


@socketio.on('connect', namespace='/ride-info')
def connect():
    emit('ride', {'connexion': 'OK'})


@socketio.on('disconnect', namespace='/ride-info')
def disconnect():
    print('disconnected')


@app.route('/ride-info', methods=['POST'])
def ride_info():
    if request.method != 'POST':
        return redirect('/uber', code=302)

    maps = Maps()

    # datas from form
    start_lat = maps.get_place_location(request.form['address_start'])['latitude']
    start_lon = maps.get_place_location(request.form['address_start'])['longitude']
    end_lat = maps.get_place_location(request.form['address_destination'])['latitude']
    end_lon = maps.get_place_location(request.form['address_destination'])['longitude']

    uber = Uber((start_lat, start_lon), (end_lat, end_lon), (request.form['address_start']), (request.form['address_destination']))

    async def async_uber_rides():
        rides = await uber.get_ride_info()

    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)

    l = asyncio.get_event_loop()
    l.run_until_complete(async_uber_rides())

    return render_template("result.html")
