# vtscan

## compare prices between VTC companies

### requirements
* python 3.5

### first installation

1. create token.txt file
2. create constant.py file with variables

* CLIENT_ID = "string"
* CLIENT_SECRET = "string"
> for Uber [https://developer.uber.com]
* REDIRECT_URI = 'http://localhost:5000/callback'
* AUTHORIZE_URL = "https://login.uber.com/oauth/v2/authorize?"
* ACCESS_TOKEN_URL = "https://login.uber.com/oauth/v2/token"
* SCOPE = "string"

3. `sh start.sh`

### /start

to launch the app and create token


#### development mode

`pip install <module_name> && pip freeze > requirements.txt`

### frontend

`cd client`
`npm run watch`

### branches

* dev = dev.consolelog
* master = localhost

### tests

`locust -f application/test/[file].py --host=http://localhost:3000`

### db Sqlite3 in /db

* connection `sqlite3 database.db`
* import `.read start_db.sql` (table=users)
* list tables `.tables`
* drop table `DROP TABLE users;`
* list table content `SELECT * FROM users;`
